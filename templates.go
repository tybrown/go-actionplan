package actionplan

import (
	"embed"
	"strings"
	"text/template"
)

var (
	//go:embed templates/*.tmpl
	templatesFS embed.FS

	templates = template.Must(
		template.New("actionplan").Funcs(template.FuncMap{
			"IndentSpaces": indentSpacesString,
		}).ParseFS(templatesFS, "templates/*.tmpl"),
	)
)

func indentSpacesString(src string, n int) string {
	if src == "" || n == 0 {
		return src
	}

	indent := ""
	for i := 0; i < n; i++ {
		indent = indent + " "
	}

	lines := strings.SplitAfter(src, "\n")
	if len(lines[len(lines)-1]) == 0 {
		lines = lines[:len(lines)-1]
	}
	return strings.Join(append([]string{""}, lines...), indent)
}
