package actionplan

import (
	"bytes"
	_ "embed"
	"fmt"
	"io"
	"log"
	"testing"

	"github.com/stretchr/testify/assert"
)

var (
	simpleCreate = SimpleAction{
		Kind:      Create,
		ShortName: "Testing Create Action",
		Detail:    "Testing Create Action Detail",
	}

	simpleUpdate = SimpleAction{
		Kind:      Update,
		ShortName: "Testing Update Action",
		Detail:    "Testing Update Action Detail",
	}

	noopApplyFunc = func() error {
		return nil
	}

	//go:embed templates/plan.tmpl.create_simple.test
	simpleCreatePlanOutput string
	//go:embed templates/apply.tmpl.create_simple.test
	simpleCreateApplyOutput string
	//go:embed templates/apply.tmpl.create_force_yes.test
	simpleCreateApplyOutputForceYes string
)

// Create a custom Action that's never Appliable
type neverAction struct {
	Kind      ActionKind
	ShortName string
	ApplyFunc func() error
}

func (na neverAction) GetKind() ActionKind {
	return na.Kind
}
func (na neverAction) GetShortName() string {
	return na.ShortName
}
func (na neverAction) GetDetail() string {
	return ""
}
func (na neverAction) GetApplyFunc() func() error {
	return na.ApplyFunc
}
func (na neverAction) IsAppliable() bool {
	return false
}

func newTestingPlan(stdin io.Reader, stdout, logout io.Writer) *Plan {
	plan := New("Testing", OnError_ContinueWithWarnings)
	plan.stdin = stdin
	plan.stdout = stdout
	plan.logger = log.New(logout, "", log.LstdFlags)
	return plan
}

func TestPlan_HappyPath(t *testing.T) {
	assert := assert.New(t)
	var (
		stdin  bytes.Buffer
		stdout bytes.Buffer
		logout bytes.Buffer

		create  = simpleCreate
		created bool
	)

	plan := newTestingPlan(&stdin, &stdout, &logout)
	create.ApplyFunc = func() error {
		created = true
		return nil
	}
	err := plan.AddActions(
		create,
	)
	if !assert.NoError(err, "adding an action to a new plan should not raise an error") {
		assert.FailNow("adding action had unrecoverable error")
	}

	assert.Len(plan.actions, 1, "plan should only have 1 action so far")
	assert.False(created, "value of created should be false, until the Apply() func is called")

	// run Plan()
	err = plan.Plan()
	if !assert.NoError(err, "there should be no errors during Plan()") {
		assert.FailNow("Plan() had an unrecoverable error")
	}
	assert.Equal(simpleCreatePlanOutput, stdout.String(), "Plan output should match expected output")
	assert.False(created, "value of created should still be false after Plan()")

	// reset the stdout buffer so we can test it again during Apply()
	stdout.Reset()

	// run Apply()
	stdin.Write([]byte("yes\n"))
	err = plan.Apply()
	assert.NoError(err, "there should be no errors during Apply()")
	assert.Equal(simpleCreateApplyOutput, stdout.String(),
		"output value of Apply() before prompting should match expected output")
	assert.Len(plan.errors, 0, "the plan should have no errors in the array")
	assert.True(created, "value of created should be true after Apply()")
	assert.Equal("", stdin.String(), "stdin should be read and empty after Apply()")
}

func TestPlan_Immutablility(t *testing.T) {
	assert := assert.New(t)
	var (
		stdin  bytes.Buffer
		stdout bytes.Buffer
		logout bytes.Buffer

		create = simpleCreate
	)

	plan := newTestingPlan(&stdin, &stdout, &logout)
	assert.True(plan.mutable, "plan should be mutable immediately after New()")

	create.ApplyFunc = func() error {
		return nil
	}
	err := plan.AddActions(
		create,
	)
	assert.True(plan.mutable, "plan should be mutable after adding any actions")
	assert.NoError(err, "no errors returned when plan is mutable")

	// Plan() should make plan immutable
	plan.Plan()
	assert.False(plan.mutable, "plan should be immutable after Plan() is called")

	err = plan.AddActions(
		SimpleAction{},
	)
	assert.ErrorIs(err, ErrPlanNotMutable, "AddActions should return an error if plan is immutable")
	assert.Len(plan.actions, 1, "length of Actions should still be 1 after AddActions returns an error")

	// manually set plan.mutable to true again to test that Apply() makes it immutable
	plan.mutable = true
	stdin.Write([]byte("yes\n"))
	plan.Apply()
	assert.False(plan.mutable, "Apply() should make plan immutable")
}

func TestPlan_OnErrorBehavior(t *testing.T) {
	onErrorTests := []struct {
		onErrorType   OnErrorType
		expectedError error
		createdState  bool
		updatedState  bool
	}{
		{OnError_AbortRemainingRun, ErrApplyAborted, true, false},
		{OnError_ContinueWithWarnings, ErrErrorsDuringApply, true, true},
	}

	for _, tt := range onErrorTests {
		tt := tt
		t.Run(tt.onErrorType.String(), func(t *testing.T) {
			assert := assert.New(t)
			var (
				stdin  bytes.Buffer
				stdout bytes.Buffer
				logout bytes.Buffer

				create  = simpleCreate
				created bool

				update  = simpleUpdate
				updated bool
			)

			plan := newTestingPlan(&stdin, &stdout, &logout)
			plan.onError = tt.onErrorType

			create.ApplyFunc = func() error {
				created = true
				// Intentionally return an error during create
				return fmt.Errorf("testing error")
			}
			update.ApplyFunc = func() error {
				updated = true
				return nil
			}
			plan.AddActions(
				create,
				update,
			)

			assert.Len(plan.actions, 2, "plan should have 2 actions")
			assert.False(created)
			assert.False(updated)

			// run Apply()
			stdin.Write([]byte("yes\n"))
			err := plan.Apply()
			assert.ErrorIs(err, tt.expectedError)
			errs := plan.GetErrors()
			assert.Len(errs, 1, "the plan should have an error in the slice")
			assert.ErrorContains(errs[0], "testing error")
			assert.Equal(tt.createdState, created)
			assert.Equal(tt.updatedState, updated)
		})
	}
}

func TestPlan_OnErrorContinueWithWarnings_CorrectLogging(t *testing.T) {
	assert := assert.New(t)
	var (
		stdin  bytes.Buffer
		stdout bytes.Buffer
		logout bytes.Buffer

		create = simpleCreate
		update = simpleUpdate
	)

	plan := newTestingPlan(&stdin, &stdout, &logout)
	plan.onError = OnError_ContinueWithWarnings

	create.ApplyFunc = func() error {
		// Intentionally return an error during create
		return fmt.Errorf("testing error")
	}
	update.ApplyFunc = func() error {
		return nil
	}
	plan.AddActions(
		create,
		update,
	)

	// run Apply()
	stdin.Write([]byte("yes\n"))
	err := plan.Apply()
	assert.ErrorIs(err, ErrErrorsDuringApply)
	errs := plan.GetErrors()
	assert.Len(errs, 1, "the plan should have an error in the slice")

	// check for the correct logging
	assert.Contains(logout.String(), fmt.Sprintf("%s %s", create.GetKind().Verb, create.GetShortName()))
	assert.NotContains(logout.String(), fmt.Sprintf("%s %s", create.GetKind().PastTenseVerb, create.GetShortName()))
	assert.Contains(logout.String(), fmt.Sprintf("%s %s", update.GetKind().Verb, update.GetShortName()))
	assert.Contains(logout.String(), fmt.Sprintf("%s %s", update.GetKind().PastTenseVerb, update.GetShortName()))
}

func TestPlan_TemplateCountByKind(t *testing.T) {
	assert := assert.New(t)
	plan := newTestingPlan(nil, nil, nil)

	err := plan.AddActions(
		SimpleAction{Kind: Create, ApplyFunc: noopApplyFunc},
		SimpleAction{Kind: Update, ApplyFunc: noopApplyFunc},
		SimpleAction{Kind: Update, ApplyFunc: noopApplyFunc},
	)
	assert.NoError(err)

	expected := map[ActionKind]int{
		Create: 1,
		Update: 2,
	}
	assert.Equal(expected, plan.TemplateCountByKind())
}

func TestPlan_ActionNotAppliable(t *testing.T) {
	assert := assert.New(t)
	var (
		stdin  bytes.Buffer
		stdout bytes.Buffer
		logout bytes.Buffer

		create     = simpleCreate
		created    = false
		update     = simpleUpdate
		updated    = false
		notUpdated = false
	)

	plan := newTestingPlan(&stdin, &stdout, &logout)

	create.ApplyFunc = func() error {
		created = true
		return nil
	}
	update.ApplyFunc = func() error {
		updated = true
		return nil
	}

	err := plan.AddActions(
		create,
		update,
		neverAction{Kind: Create, ShortName: "Create Never", ApplyFunc: func() error {
			notUpdated = true
			return nil
		}},
	)
	assert.NoError(err)

	expected := map[ActionKind]int{
		Create: 1,
		Update: 1,
	}
	assert.Equal(expected, plan.TemplateCountByKind())

	{
		stdin.Write([]byte("yes\n"))
		err := plan.Apply()
		assert.NoError(err)
		assert.True(created)
		assert.True(updated)
		assert.False(notUpdated, "This action has IsAppliable() returns false, so this value "+
			"should never have been updated")
		assert.Contains(stdout.String(), "Testing Create Action")
		assert.Contains(stdout.String(), "Testing Update Action")
		assert.NotContains(stdout.String(), "Create Never")
	}
}

func TestPlan_ForceYes(t *testing.T) {
	assert := assert.New(t)
	var (
		stdin  bytes.Buffer
		stdout bytes.Buffer
		logout bytes.Buffer

		create  = simpleCreate
		created bool
	)

	plan := newTestingPlan(&stdin, &stdout, &logout)
	plan.ForceYes = true
	create.ApplyFunc = func() error {
		created = true
		return nil
	}
	plan.AddActions(
		create,
	)

	// run Apply()
	err := plan.Apply()

	assert.NoError(err, "there should be no errors during Apply()")
	assert.Equal(simpleCreateApplyOutputForceYes, stdout.String(),
		"output value of Apply() before prompting should match expected output")
	assert.True(created, "value of created should be true after Apply()")

}

func TestPlan_AddActionWithNilApplyFunc(t *testing.T) {
	assert := assert.New(t)
	var (
		stdin  bytes.Buffer
		stdout bytes.Buffer
		logout bytes.Buffer

		create = simpleCreate
		update = simpleUpdate
	)

	plan := newTestingPlan(&stdin, &stdout, &logout)
	err := plan.AddActions(
		create,
		update,
	)
	assert.ErrorIs(err, ErrNilApplyFunc)
	assert.ErrorContains(err,
		"the GetApplyFunc() on the Action returned nil: Testing Create Action, Testing Update Action")
	assert.Len(plan.actions, 0, "actions should be empty if AddActions fails")
}

func TestPlan_ZeroActions(t *testing.T) {
	assert := assert.New(t)
	var (
		stdin  bytes.Buffer
		stdout bytes.Buffer
		logout bytes.Buffer
	)

	plan := newTestingPlan(&stdin, &stdout, &logout)

	// confirm we are in the state we expect first
	assert.Len(plan.actions, 0, "there should be zero entries in here to run the failure cases")

	// Check for the correct errors
	{
		err := plan.Plan()
		assert.ErrorIs(err, ErrZeroActionsInPlan)
	}
	{
		err := plan.Apply()
		assert.ErrorIs(err, ErrZeroActionsInPlan)
	}
}

func TestPlan_ErrorGettingConfirmationFromUser(t *testing.T) {
	assert := assert.New(t)
	var (
		stdin  bytes.Buffer
		stdout bytes.Buffer
		logout bytes.Buffer

		create = simpleCreate
		update = simpleUpdate
	)

	plan := newTestingPlan(&stdin, &stdout, &logout)
	plan.ForceYes = false // explicitly set this way for this test

	create.ApplyFunc = noopApplyFunc
	update.ApplyFunc = noopApplyFunc

	err := plan.AddActions(
		create,
		update,
	)
	assert.Len(plan.actions, 2)
	assert.NoError(err)

	{
		err := plan.Apply()
		assert.ErrorContains(err, "failed to read user input: EOF")
	}
	{
		stdin.Write([]byte("fail\n"))
		err := plan.Apply()
		assert.ErrorContains(err, "did not receive expected input, received: fail")
	}
}
