package actionplan

// Action is an interface type that's passed into Plan.AddAction. An interface type was chosen so that
// the user may implement additional logic at their own discretion.
type Action interface {
	// Kind is used to determine how the Action is displayed in the plan output.
	GetKind() ActionKind

	// ShortName is used as the primary identifier of the Action, and is referenced in the log output.
	GetShortName() string

	// Detail is meant to provide additional context to the user, such as a diff output.
	GetDetail() string

	// Appliable determines whether the Plan needs to actually execute the ApplyFunc of the Action.
	//
	// Note: This evaluation happens twice, once during the plan phase, and again during
	// apply. It is up to the user to store the result and handle that in the IsAppliable() function
	// if it's necessary to ensure that the result does not change.
	IsAppliable() bool

	// ApplyFunc is the function that will be executed during the Apply phase.
	GetApplyFunc() func() error
}

// SimpleAction is an implemention of an Action that is always Appliable
type SimpleAction struct {
	Kind      ActionKind
	ShortName string
	Detail    string
	ApplyFunc func() error
}

// GetKind implements the Action interface
func (sa SimpleAction) GetKind() ActionKind {
	return sa.Kind
}

// GetShortName implements the Action interface
func (sa SimpleAction) GetShortName() string {
	return sa.ShortName
}

// GetDetail implements the Action interface
func (sa SimpleAction) GetDetail() string {
	return sa.Detail
}

// IsAppliable ipmlements the Action interface, and always returns true
func (sa SimpleAction) IsAppliable() bool {
	return true
}

// GetApplyFunc implements the Action interface
func (sa SimpleAction) GetApplyFunc() func() error {
	return sa.ApplyFunc
}
