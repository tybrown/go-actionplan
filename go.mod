module gitlab.com/tybrown/go-actionplan

go 1.16

require (
	github.com/fatih/color v1.13.0
	github.com/stretchr/testify v1.8.0
)
