# actionplan

[![GoDoc](https://img.shields.io/badge/pkg.go.dev-doc-blue)](http://pkg.go.dev/gitlab.com/tybrown/go-actionplan)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/tybrown/go-actionplan)](https://goreportcard.com/report/gitlab.com/tybrown/go-actionplan)
[![pipeline status](https://gitlab.com/tybrown/go-actionplan/badges/main/pipeline.svg)](https://gitlab.com/tybrown/go-actionplan/-/commits/main)
[![coverage report](https://gitlab.com/tybrown/go-actionplan/badges/main/coverage.svg)](https://gitlab.com/tybrown/go-actionplan/-/graphs/main/charts)
[![Latest Release](https://gitlab.com/tybrown/go-actionplan/-/badges/release.svg)](https://gitlab.com/tybrown/go-actionplan/-/releases/permalink/latest)

A library for creating plannable/dry-runnable CLI utilities.

## Usage

```go
package main

import (
	"flag"
	"fmt"
	"os"
	"time"

	"gitlab.com/tybrown/go-actionplan"
)

var (
	dryRun = flag.Bool("dry-run", false, "Only plan, do not apply")
)

func main() {
	flag.Parse()

	plan := actionplan.New("Example Tool", actionplan.OnError_ContinueWithWarnings)

	err := plan.AddActions(
		actionplan.SimpleAction{
			Kind:      actionplan.Create,
			ShortName: "Example Create Action",
			Detail:    "Additional Create Action Details\nWith A Second Line",
			ApplyFunc: func() error {
				time.Sleep(2 * time.Second)
				return nil
			},
		},
		actionplan.SimpleAction{
			Kind:      actionplan.Update,
			ShortName: "Example Update Action",
			Detail:    "Additional Update Action Details\nWith A Second Line",
			ApplyFunc: func() error {
				time.Sleep(2 * time.Second)
				return nil
			},
		},
	)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	if *dryRun {
		if err := plan.Plan(); err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
	} else {
		if err := plan.Apply(); err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
	}
}
```
Which will generate the output:
```
$ go run main.go
Example Tool action plan has been generated.
-------------------------

  +  Example Create Action
        Additional Create Action Details
        With A Second Line

  ~  Example Update Action
        Additional Update Action Details
        With A Second Line

-------------------------
Summary of Actions by Kind:
  + Create: 1
  ~ Update in place: 1
-------------------------

  Do you want to perform these actions?
  Only 'yes' will be accepted to approve.

  Enter a value: yes

2023/02/01 17:16:16 Creating Example Create Action
2023/02/01 17:16:19 Created Example Create Action
2023/02/01 17:16:19 Updating in place Example Update Action
2023/02/01 17:16:21 Updated in place Example Update Action
```
And a dry run would look like:
```
$ go run main.go -dry-run
Example Tool action plan has been generated.
-------------------------

  +  Example Create Action
        Additional Create Action Details
        With A Second Line

  ~  Example Update Action
        Additional Update Action Details
        With A Second Line

-------------------------
Summary of Actions by Kind:
  + Create: 1
  ~ Update in place: 1
-------------------------
```

## License

MIT
