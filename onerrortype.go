package actionplan

//go:generate stringer -type=OnErrorType
type OnErrorType int

const (
	OnError_AbortRemainingRun OnErrorType = iota
	OnError_ContinueWithWarnings
)
