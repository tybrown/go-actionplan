package actionplan

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSimpleActionImplementsAction(t *testing.T) {
	assert.Implements(t, (*Action)(nil), new(SimpleAction))
}
