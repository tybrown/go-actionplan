package actionplan

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
	"sync"
)

// Plan is the type that does all the heavy lifting in this package.
// Due to the initialization required for this type, you should only use the
// New() constructor function.
// All of the methods that mutate the Plan are protected by a sync.Mutex
// to ensure thread safety. Direct modifications of struct fields require
// additional safety measures by the user.
type Plan struct {
	Name     string
	ForceYes bool

	actions []Action

	onError OnErrorType
	errors  []error

	mutable bool

	mu sync.Mutex

	stdin  io.Reader
	stdout io.Writer
	logger *log.Logger
}

// New is the required constructor for creating a Plan.
//
// `name` is used in display output to differentiate between different plan
// instances, but otherwise has no affect on how the plan functions.
//
// `onErr` determines whether the Plan will continue execution if an Action
// returns an error, or abort in the middle of the Plan.
func New(name string, onErr OnErrorType) *Plan {
	return &Plan{
		Name:    name,
		onError: onErr,
		mutable: true,

		stdin:  os.Stdin,
		stdout: os.Stdout,
		logger: log.New(os.Stdout, "", log.LstdFlags),
	}
}

// AddActions is a thread-safe method for adding types which satisfy the
// Action interface to the Plan. Actions are added sequentially in the
// same order they are added to the Plan. AddActions will return any errors
// before mutating the Actions within the plan.
func (p *Plan) AddActions(actions ...Action) error {
	p.mu.Lock()
	defer p.mu.Unlock()

	if !p.mutable {
		return ErrPlanNotMutable
	}

	nilFuncs := []string{}
	for _, action := range actions {
		if action.GetApplyFunc() == nil {
			nilFuncs = append(nilFuncs, action.GetShortName())
		}
	}
	if len(nilFuncs) != 0 {
		return fmt.Errorf("%w: %s", ErrNilApplyFunc, strings.Join(nilFuncs, ", "))
	}

	p.actions = append(p.actions, actions...)
	return nil
}

// GetActions returns a slice of all of the currently defined Actions in the plan
// This method is not protected by the sync.Mutex, so it is possible
// to receive partial results if AddActions is currently executing in another thread.
func (p *Plan) GetActions() []Action {
	return p.actions
}

// Plan method will make the Plan object immutable, and output a formatted Plan
// to os.Stdout. If there are zero Actions in the Plan when this is called, the
// Plan will be left mutable, and an ErrZeroActionsInPlan error will be returned.
func (p *Plan) Plan() error {
	p.mu.Lock()
	defer p.mu.Unlock()

	if len(p.actions) == 0 {
		return ErrZeroActionsInPlan
	}

	p.mutable = false

	planOutput, err := p.plan()
	if err != nil {
		return err
	}
	fmt.Fprint(p.stdout, planOutput)

	return nil
}

func (p *Plan) plan() (string, error) {
	var buf bytes.Buffer
	if err := templates.ExecuteTemplate(&buf, "plan", p); err != nil {
		return "", fmt.Errorf("failed to format plan template: %w", err)
	}

	return buf.String(), nil
}

// Apply method will make the Plan object immutable, output a formatted Plan
// to os.Stdout, and prompt the user for confirmation before continuing. If
// confirmation is provided by the user, Apply will iterate through the Actions
// in the plan sequentially and perform the following for each individual Action:
//   - Execute the function Action.IsAppliable() to determine if it's runnable.
//     Note: this evaluation happens twice, once during the plan phase, and again during
//     apply. It is up to the user to store the result and handle that in the IsAppliable()
//     function if it's necessary to ensure that the result does not change.
//   - Log the start of execution
//   - Execute the function returned by Action.GetApplyFunc()
//   - Check the return value for an error, log it, and either continue with execution,
//     or abort further execution depending on the `onErr` value of the Plan.
//   - Log completion of the execution
//
// If the `onErr` value is `OnError_ContinueWithWarnings`, the error will be logged
// during the execution of the Action.GetApplyFunc(), and further processing of Actions
// will continue. Apply will return `ErrErrorsDuringApply` in this case. If inspection of
// individual errors is required, `Plan.GetErrors()` method will return a `[]error` with all
// of the errors encountered.
//
// If the `onErr` value is `OnError_AbortRemainingRun`, the error will be logged
// during the execution of the Action.GetApplyFunc(), and further processing of Actions
// will halt. Apply will return `ErrApplyAborted` in this case. If inspection of the
// error is required, `Plan.GetErrors()` method will return a `[]error` with the error
// as the first element.
//
// If Plan.ForceYes is true, the user will not be prompted continue, and execution will
// commence immediately after the plan phase.
//
// If there are zero Actions in the Plan when this is called, the Plan will be left
// mutable, and an ErrZeroActionsInPlan error will be returned.
func (p *Plan) Apply() error {
	p.mu.Lock()
	defer p.mu.Unlock()

	if len(p.actions) == 0 {
		return ErrZeroActionsInPlan
	}

	p.mutable = false

	var buf bytes.Buffer
	if err := templates.ExecuteTemplate(&buf, "apply", p); err != nil {
		return fmt.Errorf("failed to format apply template: %w", err)
	}

	fmt.Fprint(p.stdout, buf.String())

	if !p.ForceYes {
		if err := p.promptToContinue(); err != nil {
			return err
		}
	}
	fmt.Fprintln(p.stdout, "") // make output nicer

	// actually run the actions
	for _, action := range p.actions {
		if !action.IsAppliable() {
			continue
		}

		f := action.GetApplyFunc()

		p.logger.Println(action.GetKind().Verb, action.GetShortName())

		err := f()
		if err != nil {
			fmt.Fprintln(p.stdout, err)
			p.logger.Println("ERROR while", action.GetKind().Verb, action.GetShortName())
			p.errors = append(p.errors, err)

			if p.onError == OnError_AbortRemainingRun {
				return ErrApplyAborted
			} else {
				continue
			}
		}
		p.logger.Println(action.GetKind().PastTenseVerb, action.GetShortName())
	}

	if len(p.errors) != 0 {
		return ErrErrorsDuringApply
	}

	return nil
}

// GetErrors returns a `[]error` with all encountered errors from each `Action`.
func (p *Plan) GetErrors() []error {
	return p.errors
}

func (p *Plan) promptToContinue() error {
	reader := bufio.NewReader(p.stdin)
	// ReadString will block until the delimiter is entered
	input, err := reader.ReadString('\n')
	if err != nil {
		return fmt.Errorf("failed to read user input: %w", err)
	}
	if input := strings.TrimSpace(input); input != "yes" {
		return fmt.Errorf("did not receive expected input, received: %s", input)
	}
	return nil
}

// TemplateCountByKind is primarily public for Plan templating purposes,
// and adds up the number of `Action`s by their `GetActionKind()`.
func (p *Plan) TemplateCountByKind() map[ActionKind]int {
	counts := map[ActionKind]int{}

	for _, action := range p.actions {
		if !action.IsAppliable() {
			continue
		}

		if _, ok := counts[action.GetKind()]; !ok {
			counts[action.GetKind()] = 0
		}
		counts[action.GetKind()]++
	}

	return counts
}
