package actionplan

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestTemplateFunc_indentSpacesString(t *testing.T) {
	assert := assert.New(t)

	testString := "foo\nbar"

	{
		ret := indentSpacesString("", 2)
		assert.Equal("", ret, "When src is empty, we should get an empty string back")
	}
	{
		ret := indentSpacesString(testString, 0)
		assert.Equal(testString, ret, "When n is 0, we should get an identical string back")
	}
	{
		ret := indentSpacesString(testString, 2)
		assert.Equal("  foo\n  bar", ret, "The returned string should be indented 2 spaces")
	}
	{
		ret := indentSpacesString(testString, 5)
		assert.Equal("     foo\n     bar", ret, "The returned string should be indented 2 spaces")
	}
	{
		ret := indentSpacesString("single line\n", 2)
		assert.Equal("  single line\n", ret, "Trailing new lines should be handed correctly")
	}
}
