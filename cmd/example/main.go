package main

import (
	"flag"
	"fmt"
	"os"
	"time"

	"gitlab.com/tybrown/go-actionplan"
)

var (
	dryRun = flag.Bool("dry-run", false, "Only plan, do not apply")
	force  = flag.Bool("force", false, "Do not prompt for confirmation before applying")
)

func main() {
	flag.Parse()

	plan := actionplan.New("Example Tool", actionplan.OnError_ContinueWithWarnings)
	plan.ForceYes = *force
	err := plan.AddActions(
		actionplan.SimpleAction{
			Kind:      actionplan.Create,
			ShortName: "Example Create Action",
			Detail:    "Additional Create Action Details\nWith A Second Line",
			ApplyFunc: func() error {
				time.Sleep(2 * time.Second)
				return nil
			},
		},
		actionplan.SimpleAction{
			Kind:      actionplan.Update,
			ShortName: "Example Update Action",
			Detail:    "Additional Update Action Details\nWith A Second Line",
			ApplyFunc: func() error {
				time.Sleep(2 * time.Second)
				return nil
			},
		},
		actionplan.SimpleAction{
			Kind:      actionplan.Delete,
			ShortName: "Example Delete Action",
			ApplyFunc: func() error {
				time.Sleep(2 * time.Second)
				return nil
			},
		},
		actionplan.SimpleAction{
			Kind:      actionplan.Recreate,
			ShortName: "Example Recreate Action",
			Detail:    "Additional Recreate Action Details\nWith A Second Line",
			ApplyFunc: func() error {
				time.Sleep(2 * time.Second)
				return nil
			},
		},
		actionplan.SimpleAction{
			Kind:      actionplan.Pause,
			ShortName: "Example Pause Action",
			Detail:    "Additional Pause Action Details\nWith A Second Line",
			ApplyFunc: func() error {
				time.Sleep(2 * time.Second)
				return nil
			},
		},
		actionplan.SimpleAction{
			Kind:      actionplan.Resume,
			ShortName: "Example Resume Action",
			Detail:    "Additional Resume Action Details\nWith A Second Line",
			ApplyFunc: func() error {
				time.Sleep(2 * time.Second)
				return nil
			},
		},
	)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	if *dryRun {
		if err := plan.Plan(); err != nil {
			fmt.Println("ERROR:", err)
			os.Exit(1)
		}
	} else {
		if err := plan.Apply(); err != nil {
			fmt.Println("ERROR:", err)
			os.Exit(1)
		}
	}
}
