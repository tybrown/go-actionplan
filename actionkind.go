package actionplan

import (
	"fmt"

	"github.com/fatih/color"
)

var (
	// Create is used to identify an Action that creates something that didn't exist
	Create = ActionKind{
		Name:          "Create",
		Verb:          "Creating",
		PastTenseVerb: "Created",
		Indicator:     color.GreenString("  +"),
	}
	// Update is used to identify an Action that updates something in-place
	Update = ActionKind{
		Name:          "Update in place",
		Verb:          "Updating in place",
		PastTenseVerb: "Updated in place",
		Indicator:     color.YellowString("  ~"),
	}
	// Delete is used to identify an Action that deletes something from existence
	Delete = ActionKind{
		Name:          "Delete",
		Verb:          "Deleting",
		PastTenseVerb: "Deleted",
		Indicator:     color.RedString("  -"),
	}
	// Recreate is used to identify an Action that deletes, and then creates a new version
	Recreate = ActionKind{
		Name:          "Recreate",
		Verb:          "Recreating",
		PastTenseVerb: "Recreated",
		Indicator:     color.RedString("-") + "/" + color.GreenString("+"),
	}
	// Pause is used to identify an Action that pauses something
	Pause = ActionKind{
		Name:          "Pause",
		Verb:          "Pausing",
		PastTenseVerb: "Paused",
		Indicator:     "  ⏸",
	}
	// Start is used to identify an Action that starts something
	Start = ActionKind{
		Name:          "Start",
		Verb:          "Starting",
		PastTenseVerb: "Started",
		Indicator:     " 🚀",
	}
	// Resume is used to identify an Action that resumes something that was paused
	Resume = ActionKind{
		Name:          "Resume",
		Verb:          "Resuming",
		PastTenseVerb: "Resumed",
		Indicator:     "▶️ ",
	}
	// Restart is used to identify an Action that restarts something
	Restart = ActionKind{
		Name:          "Restart",
		Verb:          "Restarting",
		PastTenseVerb: "Restarted",
		Indicator:     "🔁",
	}
	// Disable is used to identify an Action that disables something that was previously enabled
	Disable = ActionKind{
		Name:          "Disable",
		Verb:          "Disabling",
		PastTenseVerb: "Disabled",
		Indicator:     " ⛔️",
	}
	// Enable is used to identify an Action that enables something that was previously disabled
	Enable = ActionKind{
		Name:          "Enable",
		Verb:          "Enabling",
		PastTenseVerb: "Enabled",
		Indicator:     " ✅",
	}
	// Purge is used to identify an Action that deletes objects from a cache, queue, etc.
	Purge = ActionKind{
		Name:          "Purge",
		Verb:          "Purging",
		PastTenseVerb: "Purged",
		Indicator:     " 🗑️  ",
	}
	// Cancel is used to identify an Action that cancels something that is currently running
	Cancel = ActionKind{
		Name:          "Cancel",
		Verb:          "Cancelling",
		PastTenseVerb: "Cancelled",
		Indicator:     " ❌",
	}
)

// ActionKind determines how each individual Action will be displayed in the Plan output.
type ActionKind struct {
	// Name is expected to be a description of the action type. Example: Create
	Name string

	// Verb is used in the log output when the action begins processing. Example: Creating
	Verb string

	// PastTenseVerb is used in the log output when the action finishes processing. Example: Created
	PastTenseVerb string

	// Indicator meant to be a visually unique indicator to identify the ActionKind. Example: +
	Indicator string
}

// String implements the fmt.Stringer interface
func (kind ActionKind) String() string {
	return fmt.Sprintf("%s %s", kind.Indicator, kind.Name)
}
