package actionplan

import (
	"fmt"
)

var (
	ErrPlanNotMutable    = fmt.Errorf("unable to modify Plan, not in a mutable state")
	ErrApplyAborted      = fmt.Errorf("errors occurred during apply, aborted")
	ErrErrorsDuringApply = fmt.Errorf("errors occurred during apply, continued execution with warnings")
	ErrNilApplyFunc      = fmt.Errorf("the GetApplyFunc() on the Action returned nil")
	ErrZeroActionsInPlan = fmt.Errorf("there are zero Actions added to the Plan")
)
